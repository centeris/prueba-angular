import { UserInterface } from "./user.interface";

export class ShipInterface{
    id : number = 0;
    model : string = '';
    serie : string = '';
    description : string = '';
    capacity : number = 0;
    create? : UserInterface;
}