export class UserInterface{
    id : number = 0;
    name : string = '';
    email : string = '';
    password : string = '';
    role_id : number = 1;
}