import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private auth : AuthService, private router : Router){}

  canActivate(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    
    if(!this.canAccess(state.url)){
      this.router.navigateByUrl('/home')
    }
      
    if(this.auth.isLogIn())
      return true;
    else
      this.router.navigateByUrl('/login')
    return false;
  }

  canAccess(route : string): Boolean{
    if(route != '/home'){
      this.auth.profile().subscribe((response : any) => {
        let canAccess = false;
          response['roles'].forEach( (role : any) => {
            role['perms'].forEach( (perm : any) => {
                if(route.includes(perm['name']) && !canAccess){
                  canAccess = true;
                }
            });
          });
          if(!canAccess)
            this.router.navigateByUrl('/home')
          return true;
      }, (error) => {
        this.auth.logout();
        return false;
      });
    }
    return true;
  }
 
}
