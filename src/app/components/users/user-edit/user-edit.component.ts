import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserInterface } from 'src/app/interfaces/user.interface';
import { UsersService } from 'src/app/services/users.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styles: [
  ]
})
export class UserEditComponent implements OnInit {

  
  user : UserInterface = new UserInterface();
  id : number = 0;

  constructor(private router : Router, private userService : UsersService, private activatedRoute : ActivatedRoute) {
    this.activatedRoute.params.subscribe((params : any) => {
      this.id = params['id'];
    });
   }

  ngOnInit(): void {
    this.userService.getUser(this.id).subscribe((response) => {
      this.user = response as UserInterface;
    })
  }

  update(form : NgForm){
    if(form.invalid)
    return;
    this.userService.update(this.user).subscribe(
      (response : any) => {
          Swal.fire({
            icon: 'success',
            title: 'Éxito',
            text: "Se ha actualizado al usuario correctamente"
          }).then((result) => {
            this.back();
          })
      }, (error : any) =>{
        let text = '';
        if(error.status == 406){
          ['name','email','password'].forEach((item : any) => {
            if(typeof error.error['error'][item] !== 'undefined'){
              error.error['error'][item].forEach( (info : string) => {
                text += info + '<br>';
              });
            } 
          });

          Swal.fire({
            icon: 'error',
            title: 'Error',
            html: text
          });
        }
      }
    );
  }

  back(){
    this.router.navigateByUrl("/users");
  }
}
