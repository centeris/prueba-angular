import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserInterface } from 'src/app/interfaces/user.interface';
import { UsersService } from 'src/app/services/users.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor(private userService : UsersService, private router : Router) { }

  users : UserInterface[] = [];
  isLoad = true;

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers(){
    this.users = [];
    this.isLoad = true;
    this.userService.getUsers().subscribe((response) => {
      this.isLoad = false;
      this.users = response as UserInterface[]
    }, (err) => {});
  }

  createUser(){
    this.router.navigateByUrl('users/create')
  }

  editUser(id : number){
    this.router.navigate(['/users',id,'edit'])
  }

  destroy(id : number){
    Swal.fire({
      title: '¿Está seguro?',
      text: "Una vez eliminado no se podrá recuperar!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.showLoading()
        this.userService.destroy(id).subscribe((response) => {
          this.getUsers()
          Swal.close();
        }, (err) => {});
      }
    });
  }
}
