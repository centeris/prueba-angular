import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserInterface } from 'src/app/interfaces/user.interface';
import { UsersService } from 'src/app/services/users.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styles: [
  ]
})
export class UserCreateComponent implements OnInit {

  user : UserInterface = new UserInterface();

  constructor(private router : Router, private userService : UsersService) { }

  ngOnInit(): void {
  }

  create(form : NgForm){
    if(form.invalid)
    return;
    this.userService.create(this.user).subscribe(
      (response : any) => {
          Swal.fire({
            icon: 'success',
            title: 'Éxito',
            text: "Se ha registrado al usuario correctamente"
          }).then((result) => {
            this.back();
          })
      }, (error : any) =>{
        let text = '';
        if(error.status == 406){
          ['name','email','password'].forEach((item : any) => {
            if(typeof error.error['error'][item] !== 'undefined'){
              error.error['error'][item].forEach( (info : string) => {
                text += info + '<br>';
              });
            } 
          });

          Swal.fire({
            icon: 'error',
            title: 'Error',
            html: text
          });
        }
      }
    );
  }

  back(){
    this.router.navigateByUrl("/users");
  }
}
