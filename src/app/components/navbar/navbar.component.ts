import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: [
  ]
})
export class NavbarComponent implements OnInit {

  constructor(public auth : AuthService, private router : Router) { }

  dark_mode = false;

  ngOnInit(): void {
    this.dark_mode = (localStorage.getItem('dark-mode') === 'true');    
  }

  setMode(){
    this.dark_mode = !this.dark_mode;
    if(localStorage.getItem('dark-mode') === 'true'){
      localStorage.setItem('dark-mode', 'false');
    }else{
      localStorage.setItem('dark-mode', 'true');
    }
  }

  logout(){
    this.auth.logout();
    this.router.navigateByUrl('/');
  }

}
