import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <h3>Bienvenido {{user}} a Interestelar</h3>
  `,
  styles: [
  ]
})
export class HomeComponent implements OnInit {

  user : any = sessionStorage.getItem('user');

  constructor() { }

  ngOnInit(): void {
  }

}
