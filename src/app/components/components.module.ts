import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TripsComponent } from './trips/trips.component';
import { UsersComponent } from './users/users.component';
import { ShipsComponent } from './ships/ships.component';
import { LoginComponent } from './login/login.component';
import { UserCreateComponent } from './users/user-create/user-create.component';
import { UserEditComponent } from './users/user-edit/user-edit.component';
import { RolesPipe } from '../pipes/roles.pipe';
import { ShipComponent } from './ships/ship.component';
import { ShipEditComponent } from './ships/ship-edit/ship-edit.component';
import { ShipCreateComponent } from './ships/ship-create/ship-create.component';
import { HomeComponent } from './home/home.component';



@NgModule({
  declarations: [
    TripsComponent, 
    UsersComponent, 
    ShipsComponent, 
    LoginComponent, 
    UserCreateComponent, 
    UserEditComponent,
    RolesPipe,
    ShipComponent,
    ShipEditComponent,
    ShipCreateComponent,
    HomeComponent],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class ComponentsModule { }
