import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ShipInterface } from 'src/app/interfaces/ship.interface';
import { ShipsService } from 'src/app/services/ships.service';

@Component({
  selector: 'app-ship',
  templateUrl: './ship.component.html',
  styles: [
  ]
})
export class ShipComponent implements OnInit {

  @Input() ship : ShipInterface = new ShipInterface();

  @Output() shipSelected : EventEmitter<number>;

  constructor(private shipService : ShipsService, private router : Router) {
    this.shipSelected = new EventEmitter();
   }

  ngOnInit(): void {
  }

  edit(){
    this.router.navigate(['/ships',this.ship.id,'edit'])
  }

  destroy(){
    this.shipSelected.emit(this.ship.id);
    console.log(this.ship.id, "emit");
    
  }
}
