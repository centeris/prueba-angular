import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ShipInterface } from 'src/app/interfaces/ship.interface';
import { ShipsService } from 'src/app/services/ships.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-ship-create',
  templateUrl: './ship-create.component.html',
  styles: [
  ]
})
export class ShipCreateComponent implements OnInit {

  ship : ShipInterface = new ShipInterface();

  constructor(private router : Router, private shipService : ShipsService) { }

  ngOnInit(): void {
  }

  create(form : NgForm){
    if(form.invalid)
    return;
    this.shipService.create(this.ship).subscribe(
      (response : any) => {
          Swal.fire({
            icon: 'success',
            title: 'Éxito',
            text: "Se ha registrado la nave correctamente"
          }).then((result) => {
            this.back();
          })
      }, (error : any) =>{
        let text = '';
        if(error.status == 406){
          ['serie', 'model', 'capacity'].forEach((item : any) => {
            if(typeof error.error['error'][item] !== 'undefined'){
              error.error['error'][item].forEach( (info : string) => {
                text += info + '<br>';
              });
            } 
          });

          Swal.fire({
            icon: 'error',
            title: 'Error',
            html: text
          });
        }
      }
    );
  }

  back(){
    this.router.navigateByUrl("/ships");
  }

}
