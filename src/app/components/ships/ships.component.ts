import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ShipInterface } from 'src/app/interfaces/ship.interface';
import { ShipsService } from 'src/app/services/ships.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ships',
  templateUrl: './ships.component.html',
  styleUrls: ['./ships.component.css']
})
export class ShipsComponent implements OnInit {

  ships : ShipInterface[] = [];
  isLoad = true;

  constructor(private shipService : ShipsService, private router : Router) { }

  ngOnInit(): void {
    this.getShips();
  }
  
  getShips(){
    this.ships = [];
    this.isLoad = true;
    this.shipService.getUsers().subscribe((response) => {
      this.isLoad = false;
      this.ships = response as ShipInterface[]
    }, (err) => {});
  }
  
  createShip(){
    this.router.navigateByUrl('ships/create')
  }

  destroy(id : number){
    console.log(id);
    
    Swal.fire({
      title: '¿Está seguro?',
      text: "Una vez eliminado no se podrá recuperar!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.showLoading()
        this.shipService.destroy(id).subscribe((response) => {
          this.getShips()
          Swal.close();
        }, (err) => {});
      }
    });
  }
}
