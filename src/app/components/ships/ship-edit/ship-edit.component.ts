import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ShipInterface } from 'src/app/interfaces/ship.interface';
import { ShipsService } from 'src/app/services/ships.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-ship-edit',
  templateUrl: './ship-edit.component.html',
  styles: [
  ]
})
export class ShipEditComponent implements OnInit {

  ship : ShipInterface = new ShipInterface();
  id : number = 0;

   constructor(private router : Router, private shipService : ShipsService, private activatedRoute : ActivatedRoute) {
    this.activatedRoute.params.subscribe((params : any) => {
      this.id = params['id'];
    });
   }

  ngOnInit(): void {
    this.shipService.getShip(this.id).subscribe((response) => {
      this.ship = response as ShipInterface;
    })
  }

  update(form : NgForm){
    if(form.invalid)
    return;
    this.shipService.update(this.ship).subscribe(
      (response : any) => {
          Swal.fire({
            icon: 'success',
            title: 'Éxito',
            text: "Se ha actualizado la nave correctamente"
          }).then((result) => {
            this.back();
          })
      }, (error : any) =>{
        let text = '';
        if(error.status == 406){
          ['serie', 'model', 'capacity'].forEach((item : any) => {
            if(typeof error.error['error'][item] !== 'undefined'){
              error.error['error'][item].forEach( (info : string) => {
                text += info + '<br>';
              });
            } 
          });

          Swal.fire({
            icon: 'error',
            title: 'Error',
            html: text
          });
        }
      }
    );
  }

  back(){
    this.router.navigateByUrl("/ships");
  }

}
