import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserInterface } from 'src/app/interfaces/user.interface';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user : UserInterface = new UserInterface();
  constructor(private auth : AuthService, private router : Router) { }

  ngOnInit(): void {
  }

  login(form : NgForm){
    if(form.invalid) return;

    Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Espere por favor...'
    });
    Swal.showLoading();

    this.auth.login(this.user).subscribe(response => {
     Swal.close();
     this.router.navigateByUrl('/home');
    }, (err) => {
      Swal.fire('Error','Las credenciales son incorrectas','error')
    });
  }

}
