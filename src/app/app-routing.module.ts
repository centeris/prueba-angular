import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { ShipCreateComponent } from './components/ships/ship-create/ship-create.component';
import { ShipEditComponent } from './components/ships/ship-edit/ship-edit.component';
import { ShipsComponent } from './components/ships/ships.component';
import { UserCreateComponent } from './components/users/user-create/user-create.component';
import { UserEditComponent } from './components/users/user-edit/user-edit.component';
import { UsersComponent } from './components/users/users.component';
import { AuthGuard } from './guards/auth.guard';
import { GuestGuard } from './guards/guest.guard';

const routes: Routes = [
  {path: 'login', component: LoginComponent, canActivate: [GuestGuard]},
  {path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
  {path: 'users', component: UsersComponent, canActivate: [AuthGuard]},
  {path: 'users/create', component: UserCreateComponent, canActivate: [AuthGuard]},
  {path: 'users/:id/edit', component: UserEditComponent, canActivate: [AuthGuard]},
  {path: 'ships', component: ShipsComponent, canActivate: [AuthGuard]},
  {path: 'ships/create', component: ShipCreateComponent, canActivate: [AuthGuard]},
  {path: 'ships/:id/edit', component: ShipEditComponent, canActivate: [AuthGuard]},
  {path: '', pathMatch: 'full', redirectTo: 'login'},
  {path: '**', pathMatch: 'full', redirectTo: 'login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
