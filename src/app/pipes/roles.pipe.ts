import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'roles'
})
export class RolesPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): string {
    let text = '';
    switch(value){
        case '1': text = "Administrador"; break;
        case '2': text = "Piloto"; break;
        case '3': text = "Pasajero"; break;
        case null: text = ""; break;
    }
    return text;
  }

}
