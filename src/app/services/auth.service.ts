import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserInterface } from '../interfaces/user.interface';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  baseUrl = 'http://127.0.0.1:8000/api'
  accessToken : string = '';
  headers = new HttpHeaders();

  constructor(private http: HttpClient) {
    this.getToken();
    this.headers = this.headers.set('Authorization', `Bearer ${this.accessToken}`);
  }

  login(user : UserInterface){
    return this.http.post(`${this.baseUrl}/login`, user).pipe(
      map((response : any) => {
        this.loadToken(response['access_token']);
        sessionStorage.setItem("user", response['user']['name']);
        return response;
      })
    );
  }

  profile(){
    return this.http.get(`${this.baseUrl}/profile`, {headers: this.headers}).pipe(
      map((response : any) => {
        return response;
      })
    );
  }

  logout(){
    this.loadToken('');
  }

  loadToken(token : string){
    this.accessToken = token;
    sessionStorage.setItem('access-token',this.accessToken);
  }

  getToken(){
    let token = sessionStorage.getItem('access-token');
    if(token)
      this.accessToken = token;
    else
      this.accessToken = '';
  }

  isLogIn() : boolean{
    return this.accessToken.length > 2;
  }
}
