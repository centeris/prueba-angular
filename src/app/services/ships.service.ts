import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';
import { ShipInterface } from '../interfaces/ship.interface';

@Injectable({
  providedIn: 'root'
})
export class ShipsService {

  constructor(private http : HttpClient, private auth : AuthService) {}

  getUsers(){
    return this.http.get( `${this.auth.baseUrl}/ships`, {headers : this.auth.headers} ).pipe(
      map((response : any) => {
        return response['data'];
      })
    );
  }

  create(ship : ShipInterface){
    return this.http.post(`${this.auth.baseUrl}/ship/create`, ship, {headers : this.auth.headers});
  }

  getShip(id : number){
    return this.http.get( `${this.auth.baseUrl}/ship/${id}/get`, {headers : this.auth.headers} ).pipe(
      map((response : any) => {
        return response['data'];
      })
    );
  }
  
  update(ship : ShipInterface){
    return this.http.post(`${this.auth.baseUrl}/ship/update`, ship, {headers : this.auth.headers});
  }

  destroy(id : number){
    return this.http.delete( `${this.auth.baseUrl}/ship/${id}/delete`, {headers : this.auth.headers} ).pipe(
      map((response : any) => {
        return response['data'];
      })
    );
  }
}
