import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';
import { UserInterface } from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http : HttpClient, private auth : AuthService) {}

  getUsers(){
    return this.http.get( `${this.auth.baseUrl}/users`, {headers : this.auth.headers} ).pipe(
      map((response : any) => {
        return response['data'];
      })
    );
  }

  create(user : UserInterface){
    return this.http.post(`${this.auth.baseUrl}/user/create`, user, {headers : this.auth.headers});
  }
  
  update(user : UserInterface){
    return this.http.post(`${this.auth.baseUrl}/user/update`, user, {headers : this.auth.headers});
  }

  getUser(id : number){
    return this.http.get( `${this.auth.baseUrl}/user/${id}/get`, {headers : this.auth.headers} ).pipe(
      map((response : any) => {
        return response['data'];
      })
    );
  }

  destroy(id : number){
    return this.http.delete( `${this.auth.baseUrl}/user/${id}/delete`, {headers : this.auth.headers} ).pipe(
      map((response : any) => {
        return response['data'];
      })
    );
  }
}
